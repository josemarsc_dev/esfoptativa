package paciente;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import club.jsil.esf.R;

public class PacienteAdapter extends ArrayAdapter<Paciente> {

    public PacienteAdapter(Context context, ArrayList<Paciente> pacientes) {
        super(context, 0, pacientes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Paciente paciente = getItem(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.paciente_list_view, parent, false);
        }

        TextView pacienteCns = convertView.findViewById(R.id.pacienteListCns);
        TextView pacienteNome = convertView.findViewById(R.id.pacienteListNome);
        TextView pacienteMae = convertView.findViewById(R.id.pacienteListMae);
        TextView pacienteDataNascimento = convertView.findViewById(R.id.pacienteListDataNascimento);
        TextView pacienteMicroArea = convertView.findViewById(R.id.pacienteListMicroArea);
        TextView pacienteProntuario = convertView.findViewById(R.id.pacienteListProntuario);

        pacienteCns.setText(paciente.getCns());
        pacienteNome.setText(paciente.getNome());
        pacienteMae.setText(paciente.getMae());
        pacienteDataNascimento.setText(paciente.getDataNascimento());
        pacienteMicroArea.setText(Integer.toString(paciente.getMicroArea()));
        pacienteProntuario.setText(Integer.toString(paciente.getProntuario()));

        return convertView;
    }
}
