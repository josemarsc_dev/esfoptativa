package paciente;

import java.util.Date;

public class Paciente {

    private int id, prontuario, microArea;
    private String cns, nome, mae, dataNascimento;

    public Paciente() { }

    public Paciente(int id, int prontuario, int microArea, String cns, String nome, String mae, String dataNascimento) {
        this.id = id;
        this.prontuario = prontuario;
        this.microArea = microArea;
        this.cns = cns;
        this.nome = nome;
        this.mae = mae;
        this.dataNascimento = dataNascimento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProntuario() {
        return prontuario;
    }

    public void setProntuario(int prontuario) {
        this.prontuario = prontuario;
    }

    public int getMicroArea() {
        return microArea;
    }

    public void setMicroArea(int microArea) {
        this.microArea = microArea;
    }

    public String getCns() {
        return cns;
    }

    public void setCns(String cns) {
        this.cns = cns;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMae() {
        return mae;
    }

    public void setMae(String mae) {
        this.mae = mae;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
