package paciente;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import db.Database;

public class PacienteDB {

    private SQLiteDatabase db;
    private Database database;

    public PacienteDB(Context context) {
        database = new Database(context);
    }

    public int insert_update(Paciente paciente) {
        ContentValues contentValues;

        int result;

        db = database.getWritableDatabase();
        contentValues = new ContentValues();
        contentValues.put(Database.PACIENTE_CNS, paciente.getCns());
        contentValues.put(Database.PACIENTE_NOME, paciente.getNome());
        contentValues.put(Database.PACIENTE_MAE, paciente.getMae());
        contentValues.put(Database.PACIENTE_DATA_NASCIMENTO, paciente.getDataNascimento());
        contentValues.put(Database.PACIENTE_MICRO_AREA, paciente.getMicroArea());
        contentValues.put(Database.PACIENTE_PRONTUARIO, paciente.getProntuario());

        String where = Database.PACIENTE_ID + "=" + paciente.getId();

        try {
            if (paciente.getId() != 0) {
                result = db.update(Database.PACIENTE_TABLE, contentValues, where, null);
            } else {
                result = (int) db.insert(Database.PACIENTE_TABLE, null, contentValues);
            }
            db.close();
        } catch (SQLiteException ex) {
            Log.i("Error", ex.getMessage());
            if(ex.getMessage().toLowerCase().contains("SQLITE_CONSTRAINT_UNIQUE".toLowerCase())) {
                result = 2067;
            } else {
                result = -1234;
            }
        }

        return result;
    }

    public Cursor getAll() {
        db = database.getReadableDatabase();
        Cursor cursor = null;
        cursor = db.rawQuery("SELECT * FROM paciente", null);

        return cursor;
    }

    public Paciente getById(int id) {
        db = database.getReadableDatabase();
        Paciente paciente = new Paciente();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Database.PACIENTE_TABLE+ " WHERE " + Database.PACIENTE_ID + "=" + Integer.toString(id), null);

        if(cursor.moveToFirst()) {
            paciente.setId(cursor.getInt(cursor.getColumnIndex(Database.PACIENTE_ID)));
            paciente.setCns(cursor.getString(cursor.getColumnIndex(Database.PACIENTE_CNS)));
            paciente.setNome(cursor.getString(cursor.getColumnIndex(Database.PACIENTE_NOME)));
            paciente.setMae(cursor.getString(cursor.getColumnIndex(Database.PACIENTE_MAE)));
            paciente.setDataNascimento(cursor.getString(cursor.getColumnIndex(Database.PACIENTE_DATA_NASCIMENTO)));
            paciente.setMicroArea(cursor.getInt(cursor.getColumnIndex(Database.PACIENTE_MICRO_AREA)));
            paciente.setProntuario(cursor.getInt(cursor.getColumnIndex(Database.PACIENTE_PRONTUARIO)));
        }

        return paciente;
    }

    public int delete(int id) {
        int result;
        String where = Database.PACIENTE_ID + "=" + id;
        db = database.getReadableDatabase();
        result = db.delete(Database.PACIENTE_TABLE, where,null);
        db.close();
        return result;
    }
}
