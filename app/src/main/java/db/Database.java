package db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;

import user.UserDB;

public class Database extends SQLiteOpenHelper {

    //database variables
    public static final String DATABASE_NAME = "esfprontuario.db";
    public static final int DATABASE_VERSION = 1;
    public static final SQLiteDatabase.CursorFactory DATABASE_FACTORY = null;

    //table USER variables
    public static final String USER_TABLE = "user";
    public static final String USER_ID = "_id";
    public static final String USER_CNS = "cns";
    public static final String USER_NOME = "nome";
    public static final String USER_ADM = "adm";
    public static final String USER_USERNAME = "username";
    public static final String USER_PASSWORD = "password";

    //table PACIENTE VARIABLES
    public static final String PACIENTE_TABLE = "paciente";
    public static final String PACIENTE_ID = "_id";
    public static final String PACIENTE_CNS = "cns";
    public static final String PACIENTE_NOME = "nome";
    public static final String PACIENTE_MAE = "mae";
    public static final String PACIENTE_DATA_NASCIMENTO = "data_nascimento";
    public static final String PACIENTE_PRONTUARIO = "prontuario";
    public static final String PACIENTE_MICRO_AREA = "micro_area";


    public Database(Context context) {
        super(context, DATABASE_NAME, DATABASE_FACTORY, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String sqlTableUser = "CREATE TABLE " + USER_TABLE + "(" +
                USER_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                USER_CNS + " TEXT NOT NULL UNIQUE," +
                USER_NOME + " TEXT NOT NULL," +
                USER_ADM + " TEXT NOT NULL," +
                USER_USERNAME + " TEXT NOT NULL UNIQUE," +
                USER_PASSWORD + " TEXT NOT NULL DEFAULT '123')";

        String sqlTablePaciente= "CREATE TABLE " + PACIENTE_TABLE + "(" +
                PACIENTE_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                PACIENTE_CNS + " TEXT NOT NULL UNIQUE," +
                PACIENTE_NOME + " TEXT NOT NULL," +
                PACIENTE_MAE + " TEXT," +
                PACIENTE_DATA_NASCIMENTO + " TEXT NOT NULL," +
                PACIENTE_PRONTUARIO + " INTEGER NOT NULL DEFAULT 0," +
                PACIENTE_MICRO_AREA + " INTEGER NOT NULL)";

        try {
            db.execSQL(sqlTableUser);
            db.execSQL(sqlTablePaciente);

            insertDefaults(db);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + PACIENTE_TABLE);
        onCreate(db);
    }

    private boolean insertDefaults(SQLiteDatabase db) {
        String insertUser = "INSERT INTO " + USER_TABLE + "(" + USER_CNS + ", " + USER_NOME + ", " + USER_ADM + ", " + USER_USERNAME + ", " + USER_PASSWORD + ") VALUES (" +
                "\"700800090001234\", \"Josemar Silva\", \"adm\", \"josemar\", \"" + UserDB.getSha1("josemar") + "\")";

        String insertPacientes = "INSERT INTO " + PACIENTE_TABLE + " VALUES " +
                "(1, \"700011000000289\", \"Fulano de tal\", \"Maria do Carmo\", \"1997-12-16\", 1, 1),\n" +
                "(2, \"700011000002777\", \"Cicrano da Silva\", \"Maria Aparecida Sumida\", \"2013-07-05\", 1, 1),\n" +
                "(3, \"700011000001824\", \"Beltrano Souza\", \"Madalena Rodrigues\", \"2012-11-10\", 1, 1),\n" +
                "(4, \"700011000001516\", \"Maria das Dores Ferreira\", \"Augusta de Souza\", \"1981-02-26\", 1, 1),\n" +
                "(5, \"700011000005314\", \"João das Neves\", \"Francisca Souza\", \"2008-12-16\", 1, 2),\n" +
                "(6, \"700011000007902\", \"José Frederico\", \"Maria Silva\", \"1986-09-16\", 1, 2)";

        try {
            db.execSQL(insertUser);
            db.execSQL(insertPacientes);
            return true;
        } catch (SQLiteException ex) {
            Log.i("sqlite error", ex.getMessage());
            return false;
        }

    }
}
