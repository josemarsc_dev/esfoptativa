package club.jsil.esf;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import paciente.Paciente;
import paciente.PacienteDB;

public class AddEditPaciente extends AppCompatActivity {

    private static int PACIENTE_ID = 0;

    private EditText cnsET;
    private EditText nomeET;
    private EditText maeET;
    private EditText dataNascimentoET;
    private EditText microAreaET;
    private EditText prontuarioET;

    private Button saveBtn;

    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_paciente);

        cnsET = findViewById(R.id.cnsPaciente);
        nomeET = findViewById(R.id.nomePaciente);
        maeET = findViewById(R.id.maePaciente);
        dataNascimentoET = findViewById(R.id.dataNascimentoPaciente);
        microAreaET = findViewById(R.id.microAreaPaciente);
        prontuarioET = findViewById(R.id.prontuarioPaciente);

        saveBtn = findViewById(R.id.savePacienteBtn);

        linearLayout = findViewById(R.id.addEditPacienteButtonGroup);

        try {
            int id_paciente = Integer.parseInt(getIntent().getExtras().getString("paciente_id"));

            final PacienteDB pacienteDB = new PacienteDB(getBaseContext());
            final Paciente paciente = pacienteDB.getById(id_paciente);

            PACIENTE_ID = paciente.getId();
            cnsET.setText(paciente.getCns());
            nomeET.setText(paciente.getNome());
            maeET.setText(paciente.getMae());
            dataNascimentoET.setText(paciente.getDataNascimento());
            microAreaET.setText(Integer.toString(paciente.getMicroArea()));
            prontuarioET.setText(Integer.toString(paciente.getProntuario()));

            Button deleteButton = new Button(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            layoutParams.weight = 1;
            deleteButton.setLayoutParams(layoutParams);
            deleteButton.setText("Delete");
            deleteButton.setBackgroundResource(R.color.colorWarning);
            linearLayout.addView(deleteButton);

            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int deleted = pacienteDB.delete(PACIENTE_ID);
                    Toast.makeText(getApplicationContext(), "DELETED:" + Integer.toString(deleted), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });


        } catch (Exception ex) {
            linearLayout.setWeightSum(1);
            PACIENTE_ID = 0;
            cnsET.setText("10");
            nomeET.setText("dez");
            maeET.setText("dez");
            dataNascimentoET.setText("dez");
            microAreaET.setText("10");
            prontuarioET.setText("10");
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PacienteDB pacienteDB = new PacienteDB(getBaseContext());

                Paciente paciente = new Paciente();
                paciente.setId(PACIENTE_ID);
                paciente.setCns(cnsET.getText().toString());
                paciente.setNome(nomeET.getText().toString());
                paciente.setMae(maeET.getText().toString());
                paciente.setDataNascimento(dataNascimentoET.getText().toString());
                paciente.setMicroArea(Integer.parseInt(microAreaET.getText().toString()));
                paciente.setProntuario(Integer.parseInt(prontuarioET.getText().toString()));

                int result;
                result = pacienteDB.insert_update(paciente);
                String message = "";
                switch (result) {
                    case -1:
                        message = "Erro desconhecido salvar registro";
                        break;
                    case -1234:
                        message = "Erro de SQL";
                        break;
                    case 2067:
                        message = "Já existe um registro salvo com os dados fornecidos";
                        break;
                    default:
                        message = "Registro salvo com sucesso";
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                        finish();
                        break;
                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });

    }
}
