package club.jsil.esf;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Preferences {
    public static final String PREFERENCES_ID = "session-data";
    public static final String KEEP_CONNECTED = "keepConnected";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static void setValuesBoolean(Context context, String chave, boolean valor) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(chave, valor);
        editor.commit();
    }

    public static boolean getValuesBoolean(Context context, String chave) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        boolean b = preferences.getBoolean(chave, false);
        return b;
    }

    public static void setValuesInt(Context context, String chave, int valor) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(chave, valor);
        editor.commit();
    }

    public static int getValuesInt(Context context, String chave) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        int b = preferences.getInt(chave, 0);
        return b;
    }

    public static void setValuesString(Context context, String chave, String valor) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(chave, valor);
        editor.commit();
    }

    public static String getValuesString(Context context, String chave) {
        SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_ID, Context.MODE_PRIVATE);
        String b = preferences.getString(chave, "");
        return b;
    }

    public static boolean isCheckNotification(final Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean("PREF_CHECK_NOTIFICATION", false);
    }
}
