package club.jsil.esf;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import db.Database;
import paciente.Paciente;
import user.User;
import user.UserDB;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initializeDb();

        final UserDB userDB = new UserDB(getBaseContext());

        final CheckBox checkBox = (CheckBox) findViewById(R.id.checkbox);
        final EditText editTextUsername = (EditText) findViewById(R.id.usernameLogin);
        final EditText editTextPassword = (EditText) findViewById(R.id.passwordLogin);

        Button button = (Button) findViewById(R.id.buttonEntrar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();

                final User user = userDB.getByUsernameAndPassword(username, password);

                if (username.equals(user.getUsername()) && UserDB.getSha1(password).equals(user.getPassword())) {
//                if (username.equals("josemar") && password.equals("josemar")) {
                    Intent intent = new Intent(getApplicationContext(), PacienteList.class);
                    intent.putExtra("user_id", user.getId());

                    Preferences.setValuesString(getApplicationContext(), Preferences.USERNAME, username);

                    if (checkBox.isChecked()) {
                        Preferences.setValuesBoolean(getApplicationContext(), Preferences.KEEP_CONNECTED, true);
                    } else {
                        Preferences.setValuesBoolean(getApplication(), Preferences.KEEP_CONNECTED, false);
                    }

                    startActivity(intent);
                } else {
                    alert("Falha ao efetuar login");
                }
            }
        });

        if (Preferences.getValuesBoolean(getApplicationContext(), Preferences.KEEP_CONNECTED)) {
            Intent intent = new Intent(getApplicationContext(), PacienteList.class);
            intent.putExtra("username", Preferences.USERNAME);
            startActivity(intent);
        } else {
            editTextUsername.setText(Preferences.getValuesString(getApplicationContext(), Preferences.USERNAME));
            editTextPassword.setText(Preferences.getValuesString(getApplicationContext(), Preferences.PASSWORD));
        }
    }

    public void initializeDb() {
        Database db = new Database(this);
    }

    public void alert(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
