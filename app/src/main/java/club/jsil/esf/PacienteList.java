package club.jsil.esf;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import db.Database;
import paciente.Paciente;
import paciente.PacienteAdapter;
import paciente.PacienteAsyncTask;
import paciente.PacienteDB;
import user.User;
import user.UserDB;

public class PacienteList extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ListView listView;
    private TextView headerUserNome;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente_list);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fabAddPaciente);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        try {
            String username = Preferences.getValuesString(getApplicationContext(), Preferences.USERNAME);
            UserDB userDB = new UserDB(getBaseContext());
            Intent intent = getIntent();
            User user = userDB.getById(intent.getIntExtra("user_id", 0));
            Log.i("username:", user.getUsername());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadFromLocalDB();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(getApplicationContext(), Settings.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_pacientes_server) {
            loadFromServer();
        } else if (id == R.id.nav_pacientes_local) {
            loadFromLocalDB();
        } else if (id == R.id.nav_users) {
            Intent intent = new Intent(getApplicationContext(), UserList.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(getApplicationContext(), Settings.class);
            startActivity(intent);
        } else if (id == R.id.nav_exit) {
            finish();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void loadFromLocalDB() {
        PacienteDB pacienteDB = new PacienteDB(getBaseContext());

        final Cursor cursor = pacienteDB.getAll();

        String[] fields = new String[]{
                Database.PACIENTE_CNS,
                Database.PACIENTE_NOME,
                Database.PACIENTE_MAE,
                Database.PACIENTE_DATA_NASCIMENTO,
                Database.PACIENTE_MICRO_AREA,
                Database.PACIENTE_PRONTUARIO
        };
        int[] idsViews = new int[]{
                R.id.pacienteListCns,
                R.id.pacienteListNome,
                R.id.pacienteListMae,
                R.id.pacienteListDataNascimento,
                R.id.pacienteListMicroArea,
                R.id.pacienteListProntuario
        };

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                getBaseContext(),
                R.layout.paciente_list_view,
                cursor,
                fields,
                idsViews, 0);

        listView = findViewById(R.id.pacienteList);
        listView.setAdapter(adapter);

        TextView warning = findViewById(R.id.warningPaciente);

        if(cursor.getCount() == 0){
            warning.setText("Nenhum registro encontrado");
            warning.setVisibility(View.VISIBLE);
        } else {
            warning.setText("");
            warning.setVisibility(View.INVISIBLE);
        }

        FloatingActionButton fabAddPaciente = (FloatingActionButton) findViewById(R.id.fabAddPaciente);
        fabAddPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddEditPaciente.class);
                startActivity(intent);
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), AddEditPaciente.class);
                intent.putExtra("paciente_id", Long.toString(id));
                startActivity(intent);
            }
        });
    }

    private void loadFromServer() {
        HttpURLConnection:
        try {
            String json = new PacienteAsyncTask().execute().get();
            ArrayList<Paciente> pacientes = parserJSON(this, json);

            PacienteAdapter pacienteAdapter = new PacienteAdapter(this, pacientes);
            ListView pacienteListView = findViewById(R.id.pacienteList);
            pacienteListView.setAdapter(pacienteAdapter);
            pacienteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    alert("Mude para Pacientes Local para porder editar.");
                }
            });

        } catch (Exception e) {
            Log.i("httpurlconnection", e.getMessage(), e);
        }
    }

    private static ArrayList<Paciente> parserJSON(Context context, String json) throws IOException {
        ArrayList<Paciente> pacientes = new ArrayList<>();
        Paciente paciente;

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonPacientes = root.getJSONArray("pacientes");

            for (int i = 0;i < jsonPacientes.length(); i++) {
                paciente = new Paciente();
                paciente.setId(jsonPacientes.getJSONObject(i).getInt("id"));
                paciente.setCns(jsonPacientes.getJSONObject(i).getString("cns"));
                paciente.setNome(jsonPacientes.getJSONObject(i).getString("nome"));
                paciente.setMae(jsonPacientes.getJSONObject(i).getString("mae"));
                paciente.setDataNascimento(jsonPacientes.getJSONObject(i).getString("data_nascimento"));
                paciente.setMicroArea(jsonPacientes.getJSONObject(i).getInt("micro_area"));
                paciente.setProntuario(jsonPacientes.getJSONObject(i).getInt("prontuario"));
                pacientes.add(paciente);
            }

        } catch (JSONException e) {
            Log.i("erro", e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
        return pacientes;
    }

    private void alert(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
