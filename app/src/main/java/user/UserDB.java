package user;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.Nullable;
import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;

import db.Database;

public class UserDB {

    private SQLiteDatabase db;
    private Database database;

    public UserDB(Context context) {
        database = new Database(context);
    }

    public int insert_update(User user) {
        ContentValues contentValues;

        int result;

        db = database.getWritableDatabase();
        contentValues = new ContentValues();
        contentValues.put(Database.USER_CNS, user.getCns());
        contentValues.put(Database.USER_NOME, user.getNome());
        contentValues.put(Database.USER_ADM, user.getAdm());
        contentValues.put(Database.USER_USERNAME, user.getUsername());
        contentValues.put(Database.USER_PASSWORD, getSha1(user.getPassword()));

        String where = Database.USER_ID + "=" + user.getId();

        try {
            if (user.getId() != 0) {
                result = db.update(Database.USER_TABLE, contentValues, where, null);
            } else {
                result = (int) db.insert(Database.USER_TABLE, null, contentValues);
            }
            db.close();
        } catch (SQLiteException ex) {
            Log.i("Error", ex.getMessage());
            if(ex.getMessage().toLowerCase().contains("SQLITE_CONSTRAINT_UNIQUE".toLowerCase())) {
                result = 2067;
            } else {
                result = -1234;
            }
        }

        return result;
    }

    public Cursor getAll() {
        db = database.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Database.USER_TABLE, null);

        return cursor;
    }

    public User getById(int id) {
        db = database.getReadableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Database.USER_TABLE + " WHERE " + Database.USER_ID + "=" + Integer.toString(id), null);

        if(cursor.moveToFirst()) {
            user.setId(cursor.getInt(cursor.getColumnIndex(Database.USER_ID)));
            user.setCns(cursor.getString(cursor.getColumnIndex(Database.USER_CNS)));
            user.setNome(cursor.getString(cursor.getColumnIndex(Database.USER_NOME)));
            user.setAdm(cursor.getString(cursor.getColumnIndex(Database.USER_ADM)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(Database.USER_USERNAME)));
            user.setPassword(cursor.getString(cursor.getColumnIndex(Database.USER_PASSWORD)));
        }

        return user;
    }

    public User getByUsername(String username) {
        db = database.getReadableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM " + Database.USER_TABLE + " WHERE " + Database.USER_USERNAME + "=" + "\"" + username + "\"", null);

        if(cursor.moveToFirst()) {
            user.setId(cursor.getInt(cursor.getColumnIndex(Database.USER_ID)));
            user.setCns(cursor.getString(cursor.getColumnIndex(Database.USER_CNS)));
            user.setNome(cursor.getString(cursor.getColumnIndex(Database.USER_NOME)));
            user.setAdm(cursor.getString(cursor.getColumnIndex(Database.USER_ADM)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(Database.USER_USERNAME)));
            user.setPassword(cursor.getString(cursor.getColumnIndex(Database.USER_PASSWORD)));
        }

        return user;
    }

    public User getByUsernameAndPassword(String username, String password) {
        db = database.getReadableDatabase();
        User user = new User();
        Cursor cursor = db.rawQuery("SELECT * FROM " +
                Database.USER_TABLE +
                " WHERE " +
                Database.USER_USERNAME + "= \"" + username + "\" AND " +
                Database.USER_PASSWORD + "= \"" + getSha1(password )+ "\"", null);

        if(cursor.moveToFirst()) {
            user.setId(cursor.getInt(cursor.getColumnIndex(Database.USER_ID)));
            user.setCns(cursor.getString(cursor.getColumnIndex(Database.USER_CNS)));
            user.setNome(cursor.getString(cursor.getColumnIndex(Database.USER_NOME)));
            user.setAdm(cursor.getString(cursor.getColumnIndex(Database.USER_ADM)));
            user.setUsername(cursor.getString(cursor.getColumnIndex(Database.USER_USERNAME)));
            user.setPassword(cursor.getString(cursor.getColumnIndex(Database.USER_PASSWORD)));
        }

        return user;
    }

    public int delete(int id) {
        int result;
        String where = Database.USER_ID + "=" + id;
        db = database.getReadableDatabase();
        result = db.delete(Database.USER_TABLE, where,null);
        db.close();
        return result;
    }

    @Nullable
    public static final String getSha1(String msg) {
        String sha1 = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.reset();
            messageDigest.update(msg.getBytes("utf8"));
            sha1 = String.format("%040x", new BigInteger(1, messageDigest.digest()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return sha1;
    }
}
